import asyncio
import logging
import os

import aioschedule

from aiogram import Bot, Dispatcher, executor, types
import db
from db import add_reminder, close_reminder, check_reminders
from managers import manager_list

from dotenv import load_dotenv

load_dotenv()

API_TOKEN = os.environ.get("API_TOKEN")

# Логирование
logging.basicConfig(level=logging.INFO)

# Инициализация бота и диспетчера
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

# Константы, содержащие названия кнопок клавиатуры для отметки задания как выполненного или невыполненного
COMPLETE = "Выполнено"
INCOMPLETE = "Не сделано"

keyboard = types.ReplyKeyboardMarkup(
    keyboard=[[types.KeyboardButton(text=COMPLETE),
               types.KeyboardButton(text=INCOMPLETE)]],
    input_field_placeholder="Выберите функцию",
    row_width=1,
    resize_keyboard=True,
    one_time_keyboard=True,
)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    Отправить приветственное сообщение
    :param message:
    :return:
    """
    await message.answer("Здравствуйте. Это бот напоминаний.")
    if message.from_user.id in manager_list:
        await message.answer(
            "Я вижу, что вы есть в списке менеджеров.\n"
            "Чтобы поставить напоминание сотруднику, введите:\n"
            "<telegram_id сотрудника> <время на выполнение в минутах> <текст>"
        )


@dp.message_handler(lambda x: x.from_user.id in manager_list)
async def remind(message: types.Message):
    """
    Создать напоминание
    :param message:
    :return:
    """
    try:
        tel_id, timer, text = message.text.split(" ", 2)
        if add_reminder(tel_id, timer, text):
            await message.answer("Напоминание создано")
            await bot.send_message(tel_id, text, reply_markup=keyboard)
        else:
            await message.answer("У пользователя есть другое активное напоминание")
    except ValueError:
        await message.answer("Неверный формат")


@dp.message_handler()
async def answer(message: types.Message):
    """
    Зарегистрировать ответ от пользователя
    :param message:
    :return:
    """
    text = message.text
    if text == COMPLETE or text == INCOMPLETE:
        if close_reminder(message.from_user.id, text):
            await message.answer("Ваш ответ зарегистрирован")
            await notify_managers(f"Пользователь {message.from_user.id} нажал '{text}'")
        else:
            await message.answer("У вас нет активного напоминания")


async def notify_managers(text):
    """
    Уведомить всех менеджеров о событии
    :param text:
    :return:
    """
    for manager in manager_list:
        await bot.send_message(manager, text)


async def check():
    """
    Проверка пользователей и рассылка менеджерам
    :return:
    """
    users = check_reminders()
    if users:
        for user in users:
            await notify_managers(f"Пользователь {user} не дал ответ в течение установленного времени")


async def scheduler():
    """
    Каждую минуту проверять, не просрочил ли пользователь задание
    :return:
    """
    aioschedule.every(1).minute.do(check)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)


async def on_startup(_):
    """
    Запуск планировщика заданий
    :param _:
    :return:
    """
    asyncio.create_task(scheduler())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
