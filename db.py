import os
import datetime

from sqlalchemy import create_engine, Column, Integer, Boolean, String, DateTime
from sqlalchemy.orm import scoped_session, declarative_base, sessionmaker
from sqlalchemy.sql import func
from sqlalchemy.exc import IntegrityError

from dotenv import load_dotenv

load_dotenv()

HOST = str(os.getenv("HOST"))
PASSWORD = str(os.getenv("PASSWORD"))
DATABASE = str(os.getenv("DATABASE"))

engine = create_engine(f"postgresql+psycopg2://postgres:{PASSWORD}@{HOST}/{DATABASE}")

session = scoped_session(sessionmaker(bind=engine))

Base = declarative_base()
Base.query = session.query_property()


class Reminder(Base):
    __tablename__ = 'reminders'

    id = Column(Integer, primary_key=True)
    tel_id = Column(Integer)
    date = Column(DateTime, server_default=func.now())
    text = Column(String)
    answer_time = Column(Integer)

    answer = Column(String, default="")
    is_active = Column(Boolean, default=True)


Base.metadata.create_all(bind=engine)


def add_reminder(tel_id, answer_time, text):
    """
    Добавить задание
    :param tel_id:
    :param answer_time:
    :param text:
    :return:
    """
    reminder = session.query(Reminder).filter_by(tel_id=tel_id, is_active=True).first()
    if reminder is None:
        reminder = Reminder(tel_id=tel_id, answer_time=answer_time, text=text)
        session.add(reminder)
    else:
        return False

    try:
        session.commit()
        return True
    except IntegrityError:
        session.rollback()
        return False


def close_reminder(tel_id, answer):
    """
    Отметить задание как выполненное или невыполненное и перевести его в разряд неактивных
    :param tel_id:
    :param answer:
    :return:
    """
    reminder = session.query(Reminder).filter_by(tel_id=tel_id, is_active=True).first()
    if reminder:
        reminder.answer = answer
        reminder.is_active = False

    try:
        session.commit()
        return True
    except IntegrityError:
        session.rollback()
        return False


def check_reminders():
    """
    Проверка активных заданий на то, не истекло ли допустимое время их выполнения
    :return:
    """
    user_list = []
    reminders = session.query(Reminder).filter_by(is_active=True).all()
    for reminder in reminders:
        if datetime.datetime.now() - reminder.date > datetime.timedelta(minutes=reminder.answer_time):
            reminder.is_active = False
            user_list.append(reminder.tel_id)
    try:
        session.commit()
        return user_list
    except IntegrityError:
        session.rollback()
        return None
