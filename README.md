# БургерКит

## Установка и запуск проекта

- `python -m venv .venv` - создать виртуальное окружение
- `.env\Scripts\activate.bat` - активировать виртуальное окружение на Windows
- `source tutorial-env/bin/activate` - активировать виртуальное окружение на Linux/MacOS
- `pip install -r requirements.txt` - установить зависимости
- создать базу данных PostgreSQL
- создать `.env` файл и заполнить переменные `API_TOKEN`, `HOST`, `PASSWORD`, `DATABASE`
- в файле `managers.py` добавить telegram id менеджеров организации
- `python bot.py` - запустить проект
